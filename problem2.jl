### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 752543ee-62c2-45e0-984d-ebd9389756b0
begin
	using Pkg
	using PlutoUI
end

# ╔═╡ 774751e3-fbce-4466-8c7e-e3416c870fa5
Pkg.activate()

# ╔═╡ 2b3dc4da-d33c-11eb-1e30-370cb3896325
md" ## Assignment 3"

# ╔═╡ f947b23e-4b72-4b6d-aed6-dd12f9c4db28
md" ### Problem 2"

# ╔═╡ 715b5d19-6e0b-4e97-8d31-aeb76d1ecb71
md"> Write a program in the Julia programming language that:
- captures a sequential game between two players in its normal form 
- computes each player’s payoff using a mixed strategy."

# ╔═╡ ab48e7b7-13eb-42e7-8d3c-0de5beaa085d
md"## Player 1 Payoffs"

# ╔═╡ 80985789-dc63-4c71-8f94-4ae35de240c8
begin
	a1 = 2
	a2 = 3
	a3 = -1
	a4 = 2
end

# ╔═╡ 0e58cb3d-b10b-4456-847e-0ac23a68370a
player1 = ([a1 a2;a3 a4])

# ╔═╡ dd60de37-fa8f-4489-802a-9879267481d7
md" ## Player2 Payoffs"

# ╔═╡ 978b6a34-374f-46b9-828a-20fd991944da
begin
	b1 = 6
	b2 = -1
	b3 = 3
	b4 = 2
end

# ╔═╡ 63703be3-83cd-46fe-bfa3-be860d7d033c
player2 = ([b1 b2;b3 b4])

# ╔═╡ feb887c4-1ac4-4357-b173-3582439256e4
md" ## Combine Matrices"

# ╔═╡ 9effa1b7-7c1a-4d1b-8961-c4970a868a00
grid = [player1,player2]

# ╔═╡ 6a45d202-1dcd-40d4-b59d-ad868e55901e
a = ['a','b']

# ╔═╡ 7fb8c41b-8afe-4256-adb5-1f7481afcece
b = ['c','d']

# ╔═╡ b37a7d7a-fb9e-4064-9fc9-d9df8291a892
strat = [a,b]

# ╔═╡ 1fa137c6-d525-4681-919c-e0536fb65083
matrix = grid

# ╔═╡ a9d87dbd-5fdd-457b-8e9d-4ea7c4d7e555
md" ## Dominant Strategy"

# ╔═╡ 43b76230-a1f7-48dd-820a-c9e61e1662fe
function dom(matrix,strategies)
size(matrix)
	
function dominant(x,strat)
    res = 0
    for i = 1:size(x,1)
        tick = 0
        for j = 1:size(x,1)

            if (sum(x[i,:].>x[j,:]) == size(x,2)) && i != j
                tick = tick + 1
            end
        end
        if (tick == size(x,1)-1)
            res = i
            print("\n", "A dominant: ", x[i,:], " strategy ",strat[i])
        end
    end
    return res
end



dom = zeros(size(matrix,1))
for i = 1:size(matrix,1)
    print("\n", "Player ", i, ":")
    apu = dominant(matrix[i],strategies[i])
    dom[i] = apu
end

dom_w = Array{Any,1}(undef,2)

	for a = 1:size(matrix,1)
    print("\n", "dominated strategies ",a,":\n")
    for i = 1:size(matrix[a],1)
        if (i != dom[a]) && (dom[a] != 0)
            print(strategies[a][i], "\n")
        end
    end
end
end


# ╔═╡ 3c6d72ef-553d-4d1c-8347-2e87e0629dcf
with_terminal() do
dom(grid,strat)
end

# ╔═╡ 5363ce6b-8b07-41a2-a2c4-c211733878d1
md" ## Mixed Strategy"

# ╔═╡ ac52af60-4998-4e10-bd90-ab948cdf2398
function ProbsX(grid)
	
		x = (grid[1][4] - grid[1][3])/(grid[1][1]+grid[1][3])-(grid[1][2]+grid[1][3])
	
	return x
end

# ╔═╡ ab66b97d-bf1a-4926-bdf3-b30c366b7ac4
x =ProbsX(grid)

# ╔═╡ bd1d0a50-5789-4672-a532-dd4d445bbefb
x2 = 1 - x

# ╔═╡ 1ed51254-99dd-4123-93d7-0bf6f44f2a2d
function ProbsY(grid)
	
	y = (grid[2][4] - grid[2][3])/(grid[2][1]+grid[2][3])-(grid[2][2]+grid[2][3])
	
	return y
end

# ╔═╡ d1f46863-6756-4f75-92d9-4a6a41e5b1b5
y = ProbsY(grid)

# ╔═╡ 56400689-f103-4d69-a487-1535b8a40f24
y2 = 1 -y

# ╔═╡ 28e123ce-85f0-4bd9-a396-b1d124ab11c4
function value(grid)
	v = ((grid[1][1] * grid[1][2]) - (grid[1][3] * grid[1][4]))/((grid[1][1] + grid[1][2]) - (grid[1][3] + grid[1][4]))
			
			return v
end

# ╔═╡ 8570760f-eae3-49f9-98a6-5bb681127ef7
function value2(grid)
	v2 = ((grid[2][1] * grid[2][2]) - (grid[2][3] * grid[2][4]))/((grid[2][1] + grid[2][2]) - (grid[2][3] + grid[2][4]))
			
			return v2
end

# ╔═╡ b47739be-fb5f-4fd3-87e5-75db4bc517c3
V1 = value(grid)

# ╔═╡ 1db9e705-6f85-485b-b7d3-d6f2960a2798
V2 = value2(grid)

# ╔═╡ a2062fe0-7803-48ad-8de0-f133df914d2a
values = V1+V2

# ╔═╡ 14507c16-6e81-45f8-80ff-033159d2fcdb
p1 = x/grid[1][1]

# ╔═╡ 8517d411-de88-4c77-8ee1-8a4c0ff6a66d
p2 = x/grid[1][2]

# ╔═╡ 7f8b4da6-a92a-4fc0-a0b5-c9dab6edc288
p3 = x/grid[1][3]

# ╔═╡ 851385a7-06fd-422e-a3eb-29ee80a4e15d
p4 = x/grid[1][4]

# ╔═╡ da936c60-72da-46a4-9041-bdb78f373213
q1 = y/grid[2][1]

# ╔═╡ b920f052-f6bb-4c6e-9433-34e6c6e05c0d
q2 = y/grid[2][2]

# ╔═╡ f12bf856-29c3-4658-afa7-b43b13cdbde4
q3 = y/grid[2][3]

# ╔═╡ 8c1ef18d-ba64-46d8-a137-cf7a399b405d
q4 = y/grid[2][4]

# ╔═╡ 1bcb693b-af3c-4a17-88d5-b1f1a4d5f67b
md" ## Strategy Values"

# ╔═╡ 46977ace-4d28-49af-a234-91e30ad1fc57
probabilities1 = p1 + q1

# ╔═╡ fdda0ea1-9f9b-4ff0-b12c-4007c3f0fcfa
probabilities2 = p2 + q2

# ╔═╡ a781eb5a-53ac-462a-b8f6-c0688ec74fb2
probabilities3 = p3 + q3

# ╔═╡ 6184dda3-b901-4b3d-9f44-5bc3c4e5b019
probabilities4 = p4 + q4

# ╔═╡ 27aef4f8-80df-414e-a442-821d84d2a1ae
stratP =p1 + p2 + p3 + p4

# ╔═╡ d7c5eb13-c763-49e9-bed3-a7014f1bf694
stratQ = q1 + q2+ q3+ q4

# ╔═╡ Cell order:
# ╟─2b3dc4da-d33c-11eb-1e30-370cb3896325
# ╟─f947b23e-4b72-4b6d-aed6-dd12f9c4db28
# ╟─715b5d19-6e0b-4e97-8d31-aeb76d1ecb71
# ╠═752543ee-62c2-45e0-984d-ebd9389756b0
# ╠═774751e3-fbce-4466-8c7e-e3416c870fa5
# ╟─ab48e7b7-13eb-42e7-8d3c-0de5beaa085d
# ╠═80985789-dc63-4c71-8f94-4ae35de240c8
# ╠═0e58cb3d-b10b-4456-847e-0ac23a68370a
# ╟─dd60de37-fa8f-4489-802a-9879267481d7
# ╠═978b6a34-374f-46b9-828a-20fd991944da
# ╠═63703be3-83cd-46fe-bfa3-be860d7d033c
# ╟─feb887c4-1ac4-4357-b173-3582439256e4
# ╠═9effa1b7-7c1a-4d1b-8961-c4970a868a00
# ╠═6a45d202-1dcd-40d4-b59d-ad868e55901e
# ╠═7fb8c41b-8afe-4256-adb5-1f7481afcece
# ╠═b37a7d7a-fb9e-4064-9fc9-d9df8291a892
# ╠═1fa137c6-d525-4681-919c-e0536fb65083
# ╟─a9d87dbd-5fdd-457b-8e9d-4ea7c4d7e555
# ╠═43b76230-a1f7-48dd-820a-c9e61e1662fe
# ╠═3c6d72ef-553d-4d1c-8347-2e87e0629dcf
# ╟─5363ce6b-8b07-41a2-a2c4-c211733878d1
# ╠═ac52af60-4998-4e10-bd90-ab948cdf2398
# ╠═ab66b97d-bf1a-4926-bdf3-b30c366b7ac4
# ╠═bd1d0a50-5789-4672-a532-dd4d445bbefb
# ╠═1ed51254-99dd-4123-93d7-0bf6f44f2a2d
# ╠═d1f46863-6756-4f75-92d9-4a6a41e5b1b5
# ╠═56400689-f103-4d69-a487-1535b8a40f24
# ╠═28e123ce-85f0-4bd9-a396-b1d124ab11c4
# ╠═8570760f-eae3-49f9-98a6-5bb681127ef7
# ╠═b47739be-fb5f-4fd3-87e5-75db4bc517c3
# ╠═1db9e705-6f85-485b-b7d3-d6f2960a2798
# ╠═a2062fe0-7803-48ad-8de0-f133df914d2a
# ╠═14507c16-6e81-45f8-80ff-033159d2fcdb
# ╠═8517d411-de88-4c77-8ee1-8a4c0ff6a66d
# ╠═7f8b4da6-a92a-4fc0-a0b5-c9dab6edc288
# ╠═851385a7-06fd-422e-a3eb-29ee80a4e15d
# ╠═da936c60-72da-46a4-9041-bdb78f373213
# ╠═b920f052-f6bb-4c6e-9433-34e6c6e05c0d
# ╠═f12bf856-29c3-4658-afa7-b43b13cdbde4
# ╠═8c1ef18d-ba64-46d8-a137-cf7a399b405d
# ╟─1bcb693b-af3c-4a17-88d5-b1f1a4d5f67b
# ╠═46977ace-4d28-49af-a234-91e30ad1fc57
# ╠═fdda0ea1-9f9b-4ff0-b12c-4007c3f0fcfa
# ╠═a781eb5a-53ac-462a-b8f6-c0688ec74fb2
# ╠═6184dda3-b901-4b3d-9f44-5bc3c4e5b019
# ╠═27aef4f8-80df-414e-a442-821d84d2a1ae
# ╠═d7c5eb13-c763-49e9-bed3-a7014f1bf694
