### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 1695205b-b3ff-4325-9271-f309f6f41ca1
begin
	using Pkg
	using PlutoUI
	
end

# ╔═╡ 78b765d9-eac8-455c-9c9e-ab311b2cff87
begin
	using Random
	
	#Import existing push!() and pop!() method definitions to qualify our push!() and pop()! methods for export.
	import Base.push!,
	        Base.pop!,
	        Base.iterate,
	        Base.argmin,
	        Base.argmax,
	        Base.length,
	        Base.delete!;
	
	export if_, Queue, FIFOQueue, Stack, PQueue, push!, pop!, extend!, delete!,
	        iterate, length,
	        MemoizedFunction, eval_memoized_function,
	        AbstractProblem,
	        argmin, argmax, argmin_random_tie, argmax_random_tie,
	        weighted_sampler, weighted_sample_with_replacement,
	        distance, distance2,
	        RandomDeviceInstance,
	        isfunction, removeall,
	        normalize_probability_distribution,
	        mode, sigmoid, sigmoid_derivative,
	        combinations, iterable_cartesian_product,
	        weighted_choice;
	
	function if_(boolean_expression::Bool, ans1::Any, ans2::Any)
	    if (boolean_expression)
	        return ans1;
	    else
	        return ans2;
	    end
	end
	
	function distance(p1::Tuple{Number, Number}, p2::Tuple{Number, Number})
	    return sqrt(((Float64(p1[1]) - Float64(p2[1]))^2) + ((Float64(p1[2]) - Float64(p2[2]))^2));
	end
	
	function distance2(p1::Tuple{Number, Number}, p2::Tuple{Number, Number})
	    return (Float64(p1[1]) - Float64(p2[1]))^2 + (Float64(p1[2]) - Float64(p2[2]))^2;
	end
	
	
	
	function vector_add_tuples(a::Tuple, b::Tuple)
	    return map(+, a, b);
	end
	
	abstract type AbstractProblem end;
	
	RandomDeviceInstance = RandomDevice();
	
	#=
	
	    Define a Queue as an abstract DataType.
	
	    FIFOQueue, PriorityQueue, Stack are implementations of the Queue DataType.
	
	=#
	
	abstract type Queue end;
	
	#=
	
	    Stack is a Last In First Out (LIFO) Queue implementation.
	
	=#
	struct Stack <: Queue
	    array::Array{Any, 1}
	
	    function Stack()
	        return new(Array{Any, 1}());
	    end
	end
	
	# Map length function calls to underlying array in Stack
	length(s::Stack) = length(s.array);
	isempty(s::Stack) = isempty(s.array);
	
	# Map iterator function calls to underlying array in Stack
	iterate(s::Stack) = iterate(s.array);
	iterate(s::Stack, i) = iterate(s.array, i);
	
	#=
	
	    FIFOQueue is a First In First Out (FIFO) Queue implementation.
	
	=#
	struct FIFOQueue <: Queue
	    array::Array{Any, 1}
	
	    function FIFOQueue()
	        return new(Array{Any, 1}());
	    end
	end
	
	# Map length function calls to underlying array in FIFOQueue
	length(fq::FIFOQueue) = length(fq.array);
	isempty(fq::FIFOQueue) = isempty(fq.array);
	
	# Map iterator function calls to underlying array in FIFOQueue
	iterate(fq::FIFOQueue) = iterate(fq.array);
	iterate(fq::FIFOQueue, i) = iterate(fq.array, i);
	
	#=
	
	    PQueue is a Priority Queue implementation.
	
	    The array must consist of Tuple{Any, Any} such that,
	
	        -the first element is the priority of the item.
	
	        -the second element is the item.
	
	=#
	struct PQueue <: Queue
	    array::Array{Tuple{Any, Any}, 1}
	    order::Base.Order.Ordering
	
	    function PQueue(;order::Base.Order.Ordering=Base.Order.Forward)
	        return new(Array{Tuple{Any, Any}, 1}(), order);
	    end
	end
	
	# Map length function calls to underlying array in PQueue
	length(pq::PQueue) = length(pq.array);
	isempty(pq::PQueue) = isempty(pq.array);
	
	# Map iterator function calls to underlying array in PQueue
	iterate(pq::PQueue) = iterate(pq.array);
	iterate(pq::PQueue, i) = iterate(pq.array, i);
	
	#=
	
	    MemoizedFunction is a DataType that wraps the original function (.f) with a dictionary
	
	    (.values) containing the previous given arguments as keys to their computed values.
	
	=#
	
	struct MemoizedFunction
	    f::Function     #original function
	    values::Dict{Tuple{Vararg}, Any}
	
	    function MemoizedFunction(f::Function)
	        return new(f, Dict{Tuple{Vararg}, Any}());
	    end
	end
	
	function eval_memoized_function(mf::MemoizedFunction, args::Vararg{Any})
	    if (haskey(mf.values, args))
	        return mf.values[args];
	    else
	        mf.values[args] = mf.f(args...);
	        return mf.values[args];
	    end
	end
	
	#=
	
	    Define method definitions of push!(), pop()!, and extend()! for Queue implementations.
	
	=#
	
	"""
	    push!(s::Stack, i::Any)
	
	Push the given item 'i' to the end of the collection.
	"""
	function push!(s::Stack, i::Any)
	    push!(s.array, i);
	    nothing;
	end
	
	"""
	    push!(fq::FIFOQueue, i::Any)
	
	Push the given item 'i' to the end of the collection.
	"""
	function push!(fq::FIFOQueue, i::Any)
	    push!(fq.array, i);
	    nothing;
	end
	
	"""
	    pop!(s::Stack)
	
	Delete the last item of the collection and return the deleted item.
	"""
	function pop!(s::Stack)
	    return pop!(s.array);
	end
	
	"""
	    pop!(fq::FIFOQueue)
	
	Delete the first item of the collection and return the deleted item.
	"""
	function pop!(fq::FIFOQueue)
	    return popfirst!(fq.array);
	end
	
	"""
	    extend!(s1::Stack, s2::Queue)
	    extend!(s1::Stack, s2::AbstractVector)
	
	Push item(s) of s2 to the end of s1.
	"""
	function extend!(s1::Stack, s2::T) where {T <: Queue}
	    if (!(typeof(s2) <: PQueue))
	        for e in s2.array
	            push!(s1, e);
	        end
	    else
	        for e in s2.array
	            push!(s1, getindex(e, 2));
	        end
	    end
	    nothing;
	end
	
	function extend!(s1::Stack, s2::AbstractVector)
	    for e in s2
	        push!(s1, e);
	    end
	    nothing;
	end
	
	"""
	    extend!(fq1::FIFOQueue, fq2::Queue)
	    extend!(fq1::FIFOQueue, fq2::AbstractVector)
	
	Push item(s) of fq2 to the end of fq1.
	"""
	function extend!(fq1::FIFOQueue, fq2::T) where {T <: Queue}
	    if (!(typeof(fq2) <: PQueue))
	        for e in fq2.array
	            push!(fq1, e);
	        end
	    else
	        for e in fq2.array
	            push!(fq1, getindex(e, 2));
	        end
	    end
	    nothing;
	end
	
	function extend!(fq1::FIFOQueue, fq2::AbstractVector)
	    for e in fq2
	        push!(fq1, e);
	    end
	    nothing;
	end
	
	# Modified sorted binary search for array of tuples
	#   https://github.com/JuliaLang/julia/blob/master/base/sort.jl
	#       searchsortedfirst(), searchsortedlast(), and searchsorted()
	#
	#       These 3 functions were renamed to avoid confusion.
	#
	#
	# Base.Order.Forward will make the PQueue ordered by minimums.
	# Base.Order.Reverse will make the PQueue ordered by maximums.
	function bisearchfirst(v::Array{T, 1}, x::T, lo::Int, hi::Int, o::Base.Sort.Ordering) where {T <: Tuple{Any, Any}}
	    lo = lo-1;
	    hi = hi+1;
	    @inbounds while (lo < hi-1)
	        m = (lo+hi) >>> 1
	        if (Base.Order.lt(o, getindex(v[m], 1), getindex(x, 1)))
	            lo = m;
	        else
	            hi = m;
	        end
	    end
	    return hi;
	end
	
	function bisearchlast(v::Array{T, 1}, x::T, lo::Int, hi::Int, o::Base.Sort.Ordering) where {T <: Tuple{Any, Any}}
	    lo = lo-1;
	    hi = hi+1;
	    @inbounds while (lo < hi-1)
	        m = (lo+hi) >>> 1;
	        if (Base.Order.lt(o, getindex(x, 1), getindex(v[m], 1)))
	            hi = m;
	        else
	            lo = m;
	        end
	    end
	    return lo;
	end
	
	function bisearch(v::Array{T, 1}, x::T, ilo::Int, ihi::Int, o::Base.Sort.Ordering) where {T <: Tuple{Any, Any}}
	    lo = ilo-1;
	    hi = ihi+1;
	    @inbounds while (lo < hi-1)
	        m = (lo+hi) >>> 1;
	        if (Base.Order.lt(o, getindex(v[m], 1), getindex(x, 1)))
	            lo = m;
	        elseif (Base.Order.lt(o, getindex(x, 1), getindex(v[m], 1)))
	            hi = m;
	        else
	            a = bisearchfirst(v, x, max(lo, ilo), m, o)
	            b = bisearchlast(v, x, m, min(hi, ihi), o)
	            return a : b;
	        end
	    end
	    return (lo + 1) : (hi - 1);
	end
	
	"""
	    push!(pq::PQueue, i::Tuple{Any, Tuple})
	
	Push the given item 'i' to the index after existing entries with the same priority as getitem(i, 1).
	"""
	function push!(pq::PQueue, item::Tuple{Any, Any})
	    bsi = bisearch(pq.array, item, 1, length(pq), pq.order);
	
	    if (pq.order == Base.Order.Forward)
	        insert!(pq.array, bsi.stop + 1, item);
	    else
	        insert!(pq.array, bsi.start, item);
	    end
	    nothing;
	end
	
	function push!(pq::PQueue, item::Any, mf::MemoizedFunction)
	    local item_tuple = (eval_memoized_function(mf, item), item);
	    bsi = bisearch(pq.array, item_tuple, 1, length(pq), pq.order);
	
	    if (pq.order == Base.Order.Forward)
	        insert!(pq.array, bsi.stop + 1, item_tuple);
	    else
	        insert!(pq.array, bsi.start, item_tuple);
	    end
	    nothing;
	end
	
	function push!(pq::PQueue, item::Any, mf::Function)
	    local item_tuple = (mf(item), item);
	    bsi = bisearch(pq.array, item_tuple, 1, length(pq), pq.order);
	
	    if (pq.order == Base.Order.Forward)
	        insert!(pq.array, bsi.stop + 1, item_tuple);
	    else
	        insert!(pq.array, bsi.start, item_tuple);
	    end
	    nothing;
	end
	
	function push!(pq::PQueue, item::Any, mf::MemoizedFunction, problem::T) where {T <: AbstractProblem}
	    local item_tuple = (eval_memoized_function(mf, problem, item), item);
	    bsi = bisearch(pq.array, item_tuple, 1, length(pq), pq.order);
	
	    if (pq.order == Base.Order.Forward)
	        insert!(pq.array, bsi.stop + 1, item_tuple);
	    else
	        if (bsi.start != 0)
	            insert!(pq.array, bsi.start, item_tuple);
	        else
	            insert!(pq.array, 1, item_tuple);
	        end
	    end
	    nothing;
	end
	
	"""
	    pop!(pq::PQueue)
	
	Delete the lowest/highest item based on ordering of the collection and return the deleted item.
	"""
	function pop!(pq::PQueue)
	    return getindex(popfirst!(pq.array), 2);   #return lowest/highest priority tuple by pq.order
	end
	
	"""
	    extend!(pq1::PQueue, pq2::Queue, pv::Function)
	    extend!(pq1::PQueue, pq2::AbstractVector, pv::Function)
	
	Push item(s) of pq2 to pq1 by the priority of the item(s) returned by pv().
	"""
	function extend!(pq1::PQueue, pq2::T, pv::Function) where {T <: Queue}
	    if (!(typeof(pq2) <: PQueue))
	        for e in pq2.array
	            push!(pq1, (pv(e), e));
	        end
	    else
	        for e in pq2.array
	            push!(pq1, (pv(getindex(e, 2)), getindex(e, 2)));
	        end
	    end
	    nothing;
	end
	
	#pv - function that returns priority value
	function extend!(pq1::PQueue, pq2::AbstractVector, pv::Function)
	    for e in pq2
	        push!(pq1, (pv(e), e));
	    end
	    nothing;
	end
	
	#mpv - function that returns memoized priority value
	function extend!(pq1::PQueue, pq2::AbstractVector, mpv::MemoizedFunction)
	    for e in pq2
	        push!(pq1, (eval_memoized_function(mpv, e), e));
	    end
	    nothing;
	end
	
	function extend!(pq1::PQueue, pq2::AbstractVector, mpv::MemoizedFunction, problem::T) where {T <: AbstractProblem}
	    for e in pq2
	        push!(pq1, (eval_memoized_function(mpv, problem, e), e));
	    end
	    nothing;
	end
	
	"""
	    delete!(pq::PQueue, item::Any)
	
	Remove the item if it already exists in pq.array.
	"""
	function delete!(pq::PQueue, item::Any)
	    for (i, entry) in enumerate(pq.array)
	        if (item == getindex(entry, 2))
	            deleteat!(pq.array, i);
	            return nothing;
	        end
	    end
	    return nothing;
	end
	
	function removeall(v::String, item)
	    return replace(v, item, "");
	end
	
	function removeall(v::AbstractVector, item)
	    return collect(x for x in v if (x != item));
	end
	
	"""
	    weighted_sample_with_replacement(seq, weights, n)
	
	Return an array of 'n' elements that are chosen from 'seq' at random with replacement, with
	the probability of picking each element based on its corresponding weight in 'weights'.
	"""
	function weighted_sample_with_replacement(seq::T1, weights::T2, n::Int64) where {T1 <: Vector, T2 <: Vector}
	    local sample = weighted_sampler(seq, weights);
	    return collect(sample() for i in 1:n);
	end
	
	function weighted_sample_with_replacement(seq::String, weights::T, n::Int64) where {T <: Vector}
	    local sample = weighted_sampler(seq, weights);
	    return collect(sample() for i in 1:n);
	end
	
	"""
	    weighted_sampler(seq, weights)
	
	Return a random sample function that chooses an element from 'seq' based on its corresponding
	weight in 'weight'.
	"""
	function weighted_sampler(seq::T1, weights::T2) where {T1 <: Vector, T2 <: Vector}
	    local totals::Array{Float64, 1} = Array{Float64, 1}();
	    for w in weights
	        if (length(totals) != 0)
	            push!(totals, (w + totals[length(totals)]));
	        else
	            push!(totals, w);
	        end
	    end
	    return (function(;sequence=seq, totals_array=totals)
	                element = rand(RandomDeviceInstance)*totals_array[end];
	                bsi = searchsorted(totals_array, element);
	                if (bsi.stop == length(seq))  # Prevent indices out of bounds.
	                    return seq[bsi.stop];
	                else
	                    return seq[bsi.stop + 1];
	                end
	            end);
	end
	
	function weighted_sampler(seq::String, weights::T) where {T <: Vector}
	    local totals = Array{Any, 1}();
	    for w in weights
	        if (length(totals) != 0)
	            push!(totals, (w + totals[length(totals)]));
	        else
	            push!(totals, w);
	        end
	    end
	    return (function(;sequence=seq, totals_array=totals)
	                bsi = searchsorted(totals_array,
	                                (rand(RandomDeviceInstance)*totals_array[length(totals_array)]),
	                                1,
	                                length(totals_array),
	                                Base.Order.Forward);
	                if (bsi.stop == length(seq))  # Prevent indices out of bounds.
	                    return seq[bsi.stop];
	                else
	                    return seq[bsi.stop + 1];
	                end
	            end);
	end
	
	"""
	    argmin(seq, fn)
	
	Applies fn() to each element in seq and returns the element that has the lowest fn() value. argmin()
	is similar to mapreduce(fn, min, seq) in computing the best score, but returns the corresponding element.
	"""
	function argmin(seq::T, fn::Function) where {T <: Vector}
	    local best_element = seq[1];
	    local best_score = fn(best_element);
	    for element in seq
	        element_score = fn(element);
	        if (element_score < best_score)
	            best_element = element;
	            best_score = element_score;
	        end
	    end
	    return best_element;
	end
	
	function argmin_random_tie(seq::T, fn::Function) where {T <: Vector}
	    local best_score = fn(seq[1]);
	    local n::Int64 = 0;
	    local best_element = seq[1];
	    for element in seq
	        element_score = fn(element);
	        if (element_score < best_score)
	            best_element = element;
	            best_score = element_score;
	        elseif (element_score == best_score)
	            n = n + 1;
	            if (rand(RandomDeviceInstance, 1:n) == 1)
	                best_element = element;
	            end
	        end
	    end
	    return best_element;
	end
	
	"""
	    argmax(seq, fn)
	
	Applies fn() to each element in seq and returns the element that has the highest fn() value. argmax()
	is similar to mapreduce(fn, max, seq) in computing the best score, but returns the corresponding element.
	"""
	function argmax(seq::T, fn::Function) where {T <: Vector}
	    local best_element = seq[1];
	    local best_score = fn(best_element);
	    for element in seq
	        element_score = fn(element);
	        if (element_score > best_score)
	            best_element = element;
	            best_score = element_score;
	        end
	    end
	    return best_element;
	end
	
	function argmax_random_tie(seq::T, fn::Function) where {T <: Vector}
	    local best_score = fn(seq[1]);
	    local n::Int64 = 1;
	    local best_element = seq[1];
	    for element in seq
	        element_score = fn(element);
	        if (element_score > best_score)
	            best_element = element;
	            best_score = element_score;
	        elseif (element_score == best_score)
	            n = n + 1;
	            if (rand(RandomDeviceInstance, 1:n) == 1)
	                best_element = element;
	            end
	        end
	    end
	    return best_element;
	end
	
	"""
	    isfunction(var)
	
	Check if 'var' is callable as a function.
	"""
	function isfunction(var)
	    return (typeof(var) <: Function);
	end
	
	"""
	    normalize_probability_distribution(d)
	
	Return a collection such that each value is the corresponding value in 'd' divided
	by the sum of all values in 'd'.
	"""
	function normalize_probability_distribution(d::Dict)
	    local total::Float64 = sum(values(d));
	    for key in keys(d)
	        d[key] = d[key] / total;
	        if (!(0.0 <= d[key] <= 1.0))
	            error("normalize_probability_distribution(): ", d[key], " is not a valid probability.");
	        end
	    end
	    return dist;
	end
	
	function normalize_probability_distribution(d::AbstractVector)
	    local total::Float64 = sum(d);
	    return collect((i / total) for i in d);
	end
	
	function mode_reverse_isless(p1::Tuple, p2::Tuple)
	    return (p1[2] > p2[2]);
	end
	
	function mode(v::AbstractVector)
	    local sorted::AbstractVector = sort!(collect((i, count(j->(j == i), v)) for i in Set(v)),
	                                        lt=mode_reverse_isless);
	    if (length(sorted) == 0)
	        error("mode(): There is no mode for an empty array!");
	    else
	        return getindex(getindex(sorted, 1), 1);
	    end
	end
	
	function mode(iter::Base.Generator)
	    local sorted::AbstractVector = sort!(collect((i, count(j->(j == i), iter)) for i in Set(iter)),
	                                        lt=mode_reverse_isless);
	    if (length(sorted) == 0)
	        error("mode(): There is no mode for an empty array!");
	    else
	        return getindex(getindex(sorted, 1), 1);
	    end
	end
	
	"""
	    sigmoid(x::Number)
	
	Return the activation value of 'x' by using a sigmoid function 'S(x)' as the activation function.
	"""
	function sigmoid(x::Number)
	    return (Float64(1)/(Float64(1) + exp(-x)));
	end
	
	"""
	    sigmoid_derivative(val::Number)
	
	Return the derivative of the sigmoid function 'S(x)', where x = 'val'.
	"""
	function sigmoid_derivative(val::Number)
	    return (Float64(val) * (Float64(1) - Float64(val)));
	end
	
	# The combinations() function below was adapted from
	# https://github.com/JuliaMath/Combinatorics.jl/blob/master/src/combinations.jl
	
	"""
	    combinations(array::AbstractVector, l::Integer)
	    combinations(tuple::Tuple, l::Integer)
	    combinations(set::Set, l::Integer)
	
	Return the 'l' length subsequences of the elements in the given collection of items.
	"""
	function combinations(array::AbstractVector, l::Integer)
	    local indices::AbstractVector = collect(1:l);
	    local visited::Tuple = ();
	    local current_combination::AbstractVector;
	    if (l == 0)
	        return ([],);
	    end
	
	    if (binomial(length(array), l) > 0)
	        while (indices[1] <= length(array) - l + 1)
	            current_combination = collect(array[subseq_i] for subseq_i in indices);
	            visited = (visited..., current_combination);
	            indices = copy(indices);
	            for i in reverse(1:length(indices))
	                indices[i] = indices[i] + 1;
	                if (indices[i] > (length(array) - (length(indices) - i)))
	                    continue;
	                end
	                for j in (i + 1):lastindex(indices)
	                    indices[j] = indices[j - 1] + 1;
	                end
	                break;
	            end
	        end
	        return visited;
	    else
	        return visited;
	    end
	end
	
	function combinations(tuple::Tuple, l::Integer)
	    local indices::AbstractVector = collect(1:l);
	    local visited::Tuple = ();
	    local current_combination::AbstractVector;
	    if (l == 0)
	        return ([],);
	    end
	
	    if (binomial(length(tuple), l) > 0)
	        while (indices[1] <= length(tuple) - l + 1)
	            current_combination = collect(tuple[subseq_i] for subseq_i in indices);
	            visited = (visited..., current_combination);
	            indices = copy(indices);
	            for i in reverse(1:length(indices))
	                indices[i] = indices[i] + 1;
	                if (indices[i] > (length(tuple) - (length(indices) - i)))
	                    continue;
	                end
	                for j in (i + 1):lastindex(indices)
	                    indices[j] = indices[j - 1] + 1;
	                end
	                break;
	            end
	        end
	        return visited;
	    else
	        return visited;
	    end
	end
	
	function combinations(set::Set, l::Integer)
	    local array::AbstractVector = collect(set);
	    local indices::AbstractVector = collect(1:l);
	    local visited::Tuple = ();
	    local current_combination::AbstractVector;
	    if (l == 0)
	        return ([],);
	    end
	
	    if (binomial(length(array), l) > 0)
	        while (indices[1] <= length(array) - l + 1)
	            current_combination = collect(array[subseq_i] for subseq_i in indices);
	            visited = (visited..., current_combination);
	            indices = copy(indices);
	            for i in reverse(1:length(indices))
	                indices[i] = indices[i] + 1;
	                if (indices[i] > (length(array) - (length(indices) - i)))
	                    continue;
	                end
	                for j in (i + 1):lastindex(indices)
	                    indices[j] = indices[j - 1] + 1;
	                end
	                break;
	            end
	        end
	        return visited;
	    else
	        return visited;
	    end
	end
	
	function iterable_cartesian_product(iterable_items::AbstractVector, current_index::Int64, current_permutation::AbstractVector, product_array::AbstractVector)
	    if (current_index == length(iterable_items))
	        push!(product_array, current_permutation);
	    elseif (current_index > length(iterable_items))
	        error("iterable_cartesian_product(): The current index ", current_index, " exceeds the length of the given array!");
	    else
	        if ((typeof(iterable_items[current_index + 1]) <: Vector)
	            || (typeof(iterable_items[current_index + 1]) <: Tuple)
	            || (typeof(iterable_items[current_index + 1]) <: Set))
	            for item in iterable_items[current_index + 1]
	                iterable_cartesian_product(iterable_items, (current_index + 1), vcat(current_permutation, item), product_array);
	            end
	        else
	            error("iterable_cartesian_product(): iterable_items[", current_index + 1, "] is not iterable!");
	        end
	    end
	end
	
	function iterable_cartesian_product(iterable_items::Tuple, current_index::Int64, current_permutation::AbstractVector, product_array::AbstractVector)
	    if (current_index == length(iterable_items))
	        push!(product_array, current_permutation);
	    elseif (current_index > length(iterable_items))
	        error("iterable_cartesian_product(): The current index ", current_index, " exceeds the length of the given array!");
	    else
	        if ((typeof(iterable_items[current_index + 1]) <: Vector)
	            || (typeof(iterable_items[current_index + 1]) <: Tuple)
	            || (typeof(iterable_items[current_index + 1]) <: Set))
	            for item in iterable_items[current_index + 1]
	                iterable_cartesian_product(iterable_items, (current_index + 1), vcat(current_permutation, item), product_array);
	            end
	        else
	            error("iterable_cartesian_product(): iterable_items[", current_index + 1, "] is not iterable!");
	        end
	    end
	end
	
	"""
	    iterable_cartesian_product(iterable_items)
	
	Return the cartesian product of given items in 'iterable_items' as an array.
	"""
	function iterable_cartesian_product(iterable_items::AbstractVector)
	    local product_array::AbstractVector = [];
	    iterable_cartesian_product(iterable_items, 0, [], product_array);
	    return product_array;
	end
	
	function iterable_cartesian_product(iterable_items::Tuple)
	    local product_array::AbstractVector = [];
	    iterable_cartesian_product(iterable_items, 0, [], product_array);
	    return product_array;
	end
	
	function iterable_cartesian_product(iterable_items::Set)
	    local product_array::AbstractVector = [];
	    iterable_cartesian_product((iterable_items...), 0, [], product_array);
	    return product_array;
	end
	
	"""
	    weighted_choice(choices::AbstractVector)
	
	Return an element from the given array 'choices' based on the choice and its corresponding weight.
	"""
	function weighted_choice(choices::AbstractVector)
	    local total::Float64 = sum(collect(choice[2] for choice in choices));
	    local r::Float64 = rand(RandomDeviceInstance) * total;
	    local upto::Float64 = 0.0;
	    for (choice, weight) in choices
	        if ((upto + weight) >= r)
	            return (choice, weight);
	        end
	        upto = upto + weight;
	    end
	end
	
	
end

# ╔═╡ c289e244-d33a-11eb-046d-27e0e422042b
md" ## Assignment 3"

# ╔═╡ 3668ba4b-b0cd-49d2-9aaf-9a09cb8c07f5
md" ### Problem 1"

# ╔═╡ 8851e472-fac0-40a1-be98-8dbf72f2db9b
md" > Your task in this problem is to write a program in the Julia programming
>language that performs the following:
- capture and store a Markov decision process (MDP) at runtime;
- given a policy, evaluate the policy and improve it to get a solution."   

# ╔═╡ 910bb9f9-3bb7-4953-88aa-60f5b9042b8d
md" ## Version 1"

# ╔═╡ 9bfd74b5-a5cb-4035-aa06-dc3878a4f508
abstract type AbstractMarkovDecisionProcess end;

# ╔═╡ 19957454-eae2-40d4-b486-17ec5a925046
struct GridMarkovDecisionProcess <: AbstractMarkovDecisionProcess
    initial::Tuple{Int64, Int64}
    states::Set{Tuple{Int64, Int64}}
    actions::Set{Tuple{Int64, Int64}}
    terminal_states::Set{Tuple{Int64, Int64}}
    grid::Array{Union{Nothing, Float64}, 2}
    gamma::Float64
    reward::Dict

    function GridMarkovDecisionProcess(initial::Tuple{Int64, Int64}, terminal_states::Set{Tuple{Int64, Int64}}, grid::Array{Union{Nothing, Float64}, 2}; states::Union{Nothing, Set{Tuple{Int64, Int64}}}=nothing, gamma::Float64=0.9)
        if (!(0 < gamma <= 1))
            error("GridMarkovDecisionProcess(): The gamma variable of an MDP must be between 0 and 1, the constructor was given ", gamma, "!");
        end
        local new_states::Set{Tuple{Int64, Int64}};
        if (typeof(states) <: Set)
            new_states = states;
        else
            new_states = Set{Tuple{Int64, Int64}}();
        end
        local orientations::Set = Set{Tuple{Int64, Int64}}([(1, 0), (0, 1), (-1, 0), (0, -1)]);
        local reward::Dict = Dict();
        for i in 1:getindex(size(grid), 1)
            for j in 1:getindex(size(grid, 2))
                reward[(i, j)] = grid[i, j]
                if (!(grid[i, j] === nothing))
                    push!(new_states, (i, j));
                end
            end
        end
        return new(initial, new_states, orientations, terminal_states, grid, gamma, reward);
    end 
end

# ╔═╡ 5389a404-5778-4aef-9be5-b752e6a6fd5b
function reward(mdp::T, state) where {T <: AbstractMarkovDecisionProcess}
    return mdp.reward[state];
end

# ╔═╡ 24691c07-e6ca-4cc2-beba-38e18dbea344
function actions(mdp::T, state) where {T <: AbstractMarkovDecisionProcess}
    if (state in mdp.terminal_states)
        return Set{Nothing}([nothing]);
    else
        return mdp.actions;
    end
end

# ╔═╡ c297c662-21a7-43a3-8c48-674e79d6d601
begin
	function null_index(v::AbstractVector)
	    local i::Int64 = 0;
	    for element in v
	        i = i + 1;
	        if (element === nothing)
	            return i;
	        end
	    end
	    return -1;          
	end
	
	function index(v::Array{T, 1}, item::T) where {T <: Any}
	    local i::Int64 = 0;
	    for element in v
	        i = i + 1;
	        if (element == item)
	            return i;
	        end
	    end
	    return -1;         
	end
	
	function turn(heading::Tuple{Any, Any}, inc::Int64)
	    local o = [(1, 0), (0, 1), (-1, 0), (0, -1)];
	    return o[((index(o, heading) + inc + 3) % length(o)) + 1];
	end
end

# ╔═╡ 777352e1-2414-4334-850f-36fd4f53fcfc
function go_to(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, direction::Tuple{Int64, Int64})
    local next_state::Tuple{Int64, Int64} = map(+, state, direction);
    if (next_state in gmdp.states)
        return next_state;
    else
        return state;
    end
end

# ╔═╡ fd22ca56-5adc-4391-8f3b-58e0c50cd733
function transition_model(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, action::Nothing)
    return [(0.0, state)];
end

# ╔═╡ aaea9059-3510-4c37-b225-d969a6590d57
function transition_model(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, action::Tuple{Int64, Int64})
    return [(0.8, go_to(gmdp, state, action)),
            (0.1, go_to(gmdp, state, turn(action, -1))),
            (0.1, go_to(gmdp, state, turn(action, 1)))];
end

# ╔═╡ d057f671-aebc-4599-aaed-7428860e839b


# ╔═╡ 8a594fb9-5dd3-4e8b-8e86-b2d1283d2692

function valueIteration(mdp::T; epsilon::Float64=0.001) where {T <: AbstractMarkovDecisionProcess}
    local U_prime::Dict = Dict(collect(Pair(state, 0.0) for state in mdp.states));
    while (true)
        local U::Dict = copy(U_prime);
        local delta::Float64 = 0.0;
        for state in mdp.states
            U_prime[state] = (reward(mdp, state)
                                + (mdp.gamma
                                * max((sum(collect(p * U[state_prime] 
                                                    for (p, state_prime) in transition_model(mdp, state, action)))
                                        for action in actions(mdp, state))...)));
            delta = max(delta, abs(U_prime[state] - U[state]));
        end
        if (delta < ((epsilon * (1 - mdp.gamma))/mdp.gamma))
            return U;
        end
    end
end

# ╔═╡ 206ff1a7-911d-4c62-8818-b885cb0f32fb
function valueIteration(gmdp::GridMarkovDecisionProcess; epsilon::Float64=0.001)
    local U_prime::Dict = Dict(collect(Pair(state, 0.0) for state in gmdp.states));
    while (true)
        local U::Dict = copy(U_prime);
        local delta::Float64 = 0.0;
        for state in gmdp.states
            U_prime[state] = (reward(gmdp, state)
                            + (gmdp.gamma 
                            * max((sum(collect(p * U[state_prime] 
                                                for (p, state_prime) in transition_model(gmdp, state, action)))
                                        for action in actions(gmdp, state))...)));
            delta = max(delta, abs(U_prime[state] - U[state]));
        end
        if (delta < ((epsilon * (1 - gmdp.gamma))/gmdp.gamma))
            return U;
        end
    end
end

# ╔═╡ 336c7a34-c074-4fc2-9e61-89be92f86525
function expected_utility(mdp::T, U::Dict, state::Tuple{Int64, Int64}, action::Tuple{Int64, Int64}) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end

# ╔═╡ d5736241-4d3d-4bda-890d-152e13bc2fab
function expected_utility(mdp::T, U::Dict, state::Tuple{Int64, Int64}, action::Nothing) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end

# ╔═╡ 0070cdc9-10dc-45d0-8a19-a0cef3679bf6

function optimalPolicy(mdp::T, U::Dict) where {T <: AbstractMarkovDecisionProcess}
    local pi::Dict = Dict();
    for state in mdp.states
        pi[state] = argmax(collect(actions(mdp, state)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                return expected_utility(mdp, U, state, action);
                                                            end));
    end
    return pi;
end

# ╔═╡ 374c8be7-2706-42e4-8240-6902aee40c67

function policyEvaluation(pi::Dict, U::Dict, mdp::T; k::Int64=20) where {T <: AbstractMarkovDecisionProcess}
    for i in 1:k
        for state in mdp.states
            U[state] = (reward(mdp, state)
                        + (mdp.gamma
                        * sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, pi[state])))));
        end
    end
    return U;
end

# ╔═╡ af1ebd7d-e7a5-4ffc-9de8-ef8f9ee1557f
function policyEvaluation(pi::Dict, U::Dict, gmdp::GridMarkovDecisionProcess; k::Int64=20)
    for i in 1:k
        for state in gmdp.states
            U[state] = (reward(gmdp, state)
                        + (gmdp.gamma
                        * sum((p * U[state_prime] for (p, state_prime) in transition_model(gmdp, state, pi[state])))));
        end
    end
    return U;
end

# ╔═╡ e4035ee3-c820-4a87-b605-728a5ad40d2a

function policyIteration(mdp::T) where {T <: AbstractMarkovDecisionProcess}
    local U::Dict = Dict(collect(Pair(state, 0.0) for state in mdp.states));
    local pi::Dict = Dict(collect(Pair(state, rand(RandomDeviceInstance, collect(actions(mdp, state))))
                                    for state in mdp.states));
    while (true)
        U = policyEvaluation(pi, U, mdp);
        local unchanged::Bool = true;
        for state in mdp.states
            local action = argmax(collect(actions(mdp, state)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                    return expected_utility(mdp, U, state, action);
                                                                end));
            if (action != pi[state])
                pi[state] = action;
                unchanged = false;
            end
        end
        if (unchanged)
            return pi;
        end
    end
end

# ╔═╡ a3daa97b-b0e3-4b5f-97dd-5e0f4e53349d
md" ## initialise Environment"

# ╔═╡ 19cf97ad-00df-4cea-89ab-f84585b39f67
initialState = (1,1)

# ╔═╡ 176af57c-f14b-485f-9439-edf9384ff7c9
terminalStates = Set([(3,4),(2,4)])

# ╔═╡ e5b7120c-4551-4066-b302-cebe8657a2a5
grid = [-0.04 nothing -0.04 -0.04;-0.04 -0.04 -0.04 +1;-0.04 -0.04 -0.04 -1;-0.04 -0.04 -0.04 0]

# ╔═╡ 0188a235-03b7-4c47-9c14-00e7d287356c
function show_grid(gmdp::GridMarkovDecisionProcess, mapping::Dict)
    local grid::Array{Union{Nothing, String}, 2};
    local rows::AbstractVector = [];
    for i in 1:getindex(size(gmdp.grid), 1)
        local row::Array{Union{Nothing, String}, 1} = Array{Union{Nothing, String}, 1}();
        for j in 1:getindex(size(gmdp.grid), 2)
            push!(row, get(mapping, (i, j), nothing));
        end
        push!(rows, reshape(row, (1, length(row))));
    end
    grid = reduce(vcat, rows);
    return grid;
end

# ╔═╡ c68eeac9-c475-40c7-8958-1650f29c3186

function displayTransitions(gmdp::GridMarkovDecisionProcess, policy::Dict)
    local arrow_characters::Dict = Dict([Pair((0, 1), "right"),
                                        Pair((-1, 0), "up"),
                                        Pair((0, -1), "left"),
                                        Pair((1, 0), "down"),
                                        Pair(nothing, ".")]);
    return show_grid(gmdp, Dict(collect(Pair(state, arrow_characters[action])
                                    for (state, action) in policy)));
end

# ╔═╡ 3ee7b1f7-2120-4d2d-abc6-bfd5345393f7


# ╔═╡ d94dfd3e-e99d-40ca-a087-af6f73b45fe4
env = GridMarkovDecisionProcess(initialState,terminalStates,grid)

# ╔═╡ 02bb64ff-314a-4f4a-b7d1-05ea44392982
md" ## Solve"

# ╔═╡ 38a29f28-350e-4a76-b1ac-8fcc75a2a5cf
utilities = valueIteration(env, epsilon=0.01)

# ╔═╡ 36c6c866-1c79-4cdc-979f-dcb1068eeb04
opt = optimalPolicy(env,valueIteration(env, epsilon=0.01));

# ╔═╡ 7076fa86-b894-4365-bb8e-7bd7e86183e4
evaluatePolicy = policyEvaluation(opt,utilities,env)

# ╔═╡ 82ace500-d28c-4c05-9ce2-87bfddbd74bf
pi = policyIteration(env)

# ╔═╡ 17c50540-d355-4421-8898-4d95ab89387c
displayTransitions(env,pi)

# ╔═╡ Cell order:
# ╟─c289e244-d33a-11eb-046d-27e0e422042b
# ╟─3668ba4b-b0cd-49d2-9aaf-9a09cb8c07f5
# ╟─8851e472-fac0-40a1-be98-8dbf72f2db9b
# ╠═1695205b-b3ff-4325-9271-f309f6f41ca1
# ╟─910bb9f9-3bb7-4953-88aa-60f5b9042b8d
# ╠═9bfd74b5-a5cb-4035-aa06-dc3878a4f508
# ╠═19957454-eae2-40d4-b486-17ec5a925046
# ╠═5389a404-5778-4aef-9be5-b752e6a6fd5b
# ╠═24691c07-e6ca-4cc2-beba-38e18dbea344
# ╠═c297c662-21a7-43a3-8c48-674e79d6d601
# ╠═777352e1-2414-4334-850f-36fd4f53fcfc
# ╠═fd22ca56-5adc-4391-8f3b-58e0c50cd733
# ╠═aaea9059-3510-4c37-b225-d969a6590d57
# ╠═0188a235-03b7-4c47-9c14-00e7d287356c
# ╠═c68eeac9-c475-40c7-8958-1650f29c3186
# ╠═d057f671-aebc-4599-aaed-7428860e839b
# ╠═8a594fb9-5dd3-4e8b-8e86-b2d1283d2692
# ╠═206ff1a7-911d-4c62-8818-b885cb0f32fb
# ╠═336c7a34-c074-4fc2-9e61-89be92f86525
# ╠═d5736241-4d3d-4bda-890d-152e13bc2fab
# ╠═0070cdc9-10dc-45d0-8a19-a0cef3679bf6
# ╠═374c8be7-2706-42e4-8240-6902aee40c67
# ╠═af1ebd7d-e7a5-4ffc-9de8-ef8f9ee1557f
# ╠═78b765d9-eac8-455c-9c9e-ab311b2cff87
# ╠═e4035ee3-c820-4a87-b605-728a5ad40d2a
# ╟─a3daa97b-b0e3-4b5f-97dd-5e0f4e53349d
# ╠═19cf97ad-00df-4cea-89ab-f84585b39f67
# ╠═176af57c-f14b-485f-9439-edf9384ff7c9
# ╠═e5b7120c-4551-4066-b302-cebe8657a2a5
# ╠═3ee7b1f7-2120-4d2d-abc6-bfd5345393f7
# ╠═d94dfd3e-e99d-40ca-a087-af6f73b45fe4
# ╟─02bb64ff-314a-4f4a-b7d1-05ea44392982
# ╠═38a29f28-350e-4a76-b1ac-8fcc75a2a5cf
# ╠═36c6c866-1c79-4cdc-979f-dcb1068eeb04
# ╠═7076fa86-b894-4365-bb8e-7bd7e86183e4
# ╠═82ace500-d28c-4c05-9ce2-87bfddbd74bf
# ╠═17c50540-d355-4421-8898-4d95ab89387c
