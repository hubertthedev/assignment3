# assignment3

## Artificial Intelligence and Computer Graphics
### (AIG710S)
#### Third Assignment
- Due on 2/07/2021

- Problem 1.
- Mark: 25
> Your task in this problem is to write a program in the Julia programming
> language that performs the following:
> - capture and store a Markov decision process (MDP) at runtime;
> - given a policy, evaluate the policy and improve it to get a solution. 


- Problem 2
- Mark: 25
> Write a program in the Julia programming language that captures a 
> sequential game between two players in its extensive form and computes each
> player’s payoff using a mixed strategy.
